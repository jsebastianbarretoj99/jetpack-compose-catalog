package com.softwarejourneys.jetpackcomposecatalog

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout

@Composable
fun ConstraintExampleOne() {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
    ) {

        val (redBox, blueBox, yellowBox, magentaBox, greenBox) = createRefs()

        Box(
            modifier = Modifier
                .size(125.dp)
                .background(Color.Red)
                .constrainAs(
                    ref = redBox
                ){
                    top.linkTo(
                        anchor = parent.top
                    )
                    start.linkTo(
                        anchor = parent.start
                    )
                    end.linkTo(
                        anchor = parent.end
                    )
                    bottom.linkTo(
                        anchor = parent.bottom
                    )
                }
        )
        Box(
            modifier = Modifier
                .size(125.dp)
                .background(Color.Blue)
                .constrainAs(
                    ref = blueBox
                ){
                    top.linkTo(
                        anchor = redBox.bottom
                    )
                    start.linkTo(
                        anchor = redBox.end
                    )
                }
        )
        Box(
            modifier = Modifier
                .size(125.dp)
                .background(Color.Yellow)
                .constrainAs(
                    ref = yellowBox
                ){
                    bottom.linkTo(
                        anchor = redBox.top
                    )
                    end.linkTo(
                        anchor = redBox.start
                    )
                }
        )
        Box(
            modifier = Modifier
                .size(125.dp)
                .background(Color.Magenta)
                .constrainAs(
                    ref = magentaBox
                ){
                    bottom.linkTo(
                        anchor = redBox.top
                    )
                    start.linkTo(
                        anchor = redBox.end
                    )
                }
        )
        Box(
            modifier = Modifier
                .size(125.dp)
                .background(Color.Green)
                .constrainAs(
                    ref = greenBox
                ){
                    top.linkTo(
                        anchor = redBox.bottom
                    )
                    end.linkTo(
                        anchor = redBox.start
                    )
                }
        )
    }
}

@Composable
fun ConstraintExampleGuide() {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
    ) {
        val redBox = createRef()

        /// Guide Lines
//        val startGuideOne = createGuidelineFromTop(
//            offset = 16.dp
//        )
        val topGuide = createGuidelineFromTop(
            fraction = 0.1f
        )
        val startGuide = createGuidelineFromStart(
            fraction = 0.25f
        )

        Box(
            modifier = Modifier
                .size(125.dp)
                .background(Color.Red)
                .constrainAs(
                    ref = redBox
                ){
                    top.linkTo(
                        anchor = topGuide
                    )
                    start.linkTo(
                        anchor = startGuide
                    )
                }
        )
    }
}

@Composable
fun ConstraintExampleBarrier() {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
    ) {
        val (redBox, yellowBox, greenBox) = createRefs()

        /// Barrier
        val barrier = createEndBarrier(
            redBox, greenBox
        )

        Box(
            modifier = Modifier
                .size(125.dp)
                .background(Color.Green)
                .constrainAs(
                    ref = greenBox
                ){
                    start.linkTo(
                        anchor = parent.start,
                        margin = 16.dp
                    )
                }
        )

        Box(
            modifier = Modifier
                .size(235.dp)
                .background(Color.Red)
                .constrainAs(
                    ref = redBox
                ){
                    top.linkTo(
                        anchor = greenBox.bottom
                    )
                    start.linkTo(
                        anchor = parent.start,
                        margin = 32.dp
                    )
                }
        )

        Box(
            modifier = Modifier
                .size(50.dp)
                .background(Color.Yellow)
                .constrainAs(
                    ref = yellowBox
                ){
                    start.linkTo(
                        anchor = barrier
                    )
                }
        )
    }
}

@Preview(showBackground = true, name = "Constraint Example Chain")
@Composable
fun ConstraintExampleChain() {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
    ) {
        val (redBox, yellowBox, greenBox) = createRefs()

        Box(
            modifier = Modifier
                .size(75.dp)
                .background(Color.Green)
                .constrainAs(
                    ref = greenBox
                ){
                    start.linkTo(
                        anchor = parent.start
                    )
                    end.linkTo(
                        anchor = redBox.start
                    )
                }
        )

        Box(
            modifier = Modifier
                .size(75.dp)
                .background(Color.Red)
                .constrainAs(
                    ref = redBox
                ){
                    start.linkTo(
                        anchor = greenBox.end
                    )
                    end.linkTo(
                        anchor = yellowBox.start
                    )
                }
        )

        Box(
            modifier = Modifier
                .size(75.dp)
                .background(Color.Yellow)
                .constrainAs(
                    ref = yellowBox
                ){
                    start.linkTo(
                        anchor = redBox.end
                    )
                    end.linkTo(
                        anchor = parent.end
                    )
                }
        )

        createHorizontalChain(
            greenBox, redBox, yellowBox, chainStyle = ChainStyle.SpreadInside
        )
    }
}

@Preview(showBackground = true, name = "Constraint Example Chain Two")
@Composable
fun ConstraintExampleChainTwo() {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
    ) {
        val (redBox, yellowBox, greenBox) = createRefs()

        Box(
            modifier = Modifier
                .size(75.dp)
                .background(Color.Green)
                .constrainAs(
                    ref = greenBox
                ){
                    top.linkTo(
                        anchor = parent.top
                    )
                    bottom.linkTo(
                        anchor = redBox.top
                    )
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        )

        Box(
            modifier = Modifier
                .size(75.dp)
                .background(Color.Red)
                .constrainAs(
                    ref = redBox
                ){
                    top.linkTo(
                        anchor = greenBox.bottom
                    )
                    bottom.linkTo(
                        anchor = yellowBox.top
                    )
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        )

        Box(
            modifier = Modifier
                .size(75.dp)
                .background(Color.Yellow)
                .constrainAs(
                    ref = yellowBox
                ){
                    top.linkTo(
                        anchor = redBox.bottom
                    )
                    bottom.linkTo(
                        anchor = parent.bottom
                    )
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        )

        createVerticalChain(
            greenBox, redBox, yellowBox, chainStyle = ChainStyle.SpreadInside
        )
    }
}

