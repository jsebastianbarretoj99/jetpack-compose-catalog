package com.softwarejourneys.jetpackcomposecatalog

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.softwarejourneys.jetpackcomposecatalog.ui.theme.JetpackComposeCatalogTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JetpackComposeCatalogTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MyComplexLayout()
                }
            }
        }
    }
}

@Composable
fun MyComplexLayout(){
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(weight = 1f)
                .background(Color.Cyan),
            contentAlignment = Alignment.Center
        ){
            Text(
                text = "Ejemplo 1"
            )
        }
        MySpacer(height = 100)
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .weight(weight = 1f)
        ){
            Box(
                modifier = Modifier
                    .fillMaxHeight()
                    .weight(weight = 1f)
                    .background(Color.Red),
                contentAlignment = Alignment.Center
            ){
                Text(
                    text = "Ejemplo 2"
                )
            }
            Box(
                modifier = Modifier
                    .fillMaxHeight()
                    .weight(weight = 1f)
                    .background(Color.Green),
                contentAlignment = Alignment.Center
            ){
                Text(
                    text = "Ejemplo 3"
                )
            }
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(weight = 1f)
                .background(Color.Magenta),
            contentAlignment = Alignment.BottomCenter
        ){
            Text(
                text = "Ejemplo 4"
            )
        }

    }
}

@Composable
fun MySpacer(width: Int = 0, height: Int = 0){
    Spacer(
        modifier = Modifier
            .width(width.dp)
            .height(height.dp)
    )
}

@Composable
fun MyRow(){
//    Row(
//        modifier = Modifier
//            .fillMaxSize(),
//        horizontalArrangement = Arrangement.SpaceBetween
//    ) {
//        Text(
//            text = "Example 1",
//            modifier = Modifier
//                .background(Color.Red)
//        )
//        Text(
//            text = "Example 1",
//            modifier = Modifier
//                .background(Color.Red)
//        )
//        Text(
//            text = "Example 1",
//            modifier = Modifier
//                .background(Color.Red)
//        )
//    }
    Row(
        modifier = Modifier
            .fillMaxSize()
            .horizontalScroll(
                rememberScrollState()
            ),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .width(100.dp)
        )

    }
}

@Composable
fun MyColumn(){
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(
                rememberScrollState()
            ),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .fillMaxWidth()
                .height(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .fillMaxWidth()
                .height(100.dp)
        )
        Text(
            text = "Example 1",
            modifier = Modifier
                .background(Color.Red)
                .fillMaxWidth()
                .height(100.dp)
        )
    }
}

@Composable
fun MyBox() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ){
        Box(
            modifier = Modifier
                .width(200.dp)
                .height(200.dp)
                .background(Color.Cyan)
                .verticalScroll(
                    rememberScrollState()
                ),
            contentAlignment = Alignment.Center
        ){
            Text(text = "Hello World!")
        }
    }
}

@Preview(
    showBackground = true,
    showSystemUi = true,
)
@Composable
fun GreetingPreview() {
    JetpackComposeCatalogTheme {
        MyComplexLayout()
    }
}