# Jetpack Compose Catalog
Welcome to the Jetpack Compose Catalog repository! Here, you will find examples and demos for various key components and concepts of Jetpack Compose, the modern UI toolkit for Android.

## Description
This repository aims to serve as a hands-on guide for developers interested in learning and utilizing Jetpack Compose. It contains code examples and interactive demos that cover a variety of topics related to building user interfaces with Compose.

## Contents
The repository is organized into folders that cover different aspects of Jetpack Compose:

- Rows and Columns: Examples showcasing how to create horizontal and vertical linear layouts using Row and Column in Compose.

- Box and Complex Layouts: Demos illustrating how to use the Box layout to position and overlay elements, as well as examples of more intricate layouts involving combinations of components.

- Constraint Layouts: Examples using Compose's ConstraintLayout to create flexible and adaptive user interfaces, including barriers, guidelines, and chains.

## Usage Instructions

Each folder contains code examples along with detailed explanations in the comments. You can explore the different examples and run them in an Android Studio project with Jetpack Compose support.

1. Clone this repository to your local machine using the following command:

```sh
git clone https://gitlab.com/jsebastianbarretoj99/jetpack-compose-catalog.git

```

2. Open Android Studio and select "Open an existing Android Studio project."

3. Navigate to the location where you cloned this repository and select the project folder.

4. Explore the different folders for examples that interest you. Each folder contains its own standalone project.

5. Run the examples on the emulator or an Android device to see how they work in action.

## Contributions
Contributions are welcome! If you have additional examples, documentation improvements, or fixes, feel free to create a pull request. Make sure to follow the repository's contribution guidelines.
